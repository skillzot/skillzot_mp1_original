<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
	</div><!-- #main -->
	
	<div id="footer" role="contentinfo">
		<div class="clearfix"></div>
		<div style="border-bottom: 1px dashed #F06142;height:1px;float:left;width:100%;"></div>
		
		<div id="colophon">
		
<?php
	/* A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
	get_sidebar( 'footer' );
?>
			
			
			<div class="footercontent">
				<a href="/index/aboutus">About Us&nbsp;</a> | <a href="/index/teamhiring#ourteam">&nbsp;Team&nbsp;</a> | <a href="/index/teamhiring#weare">&nbsp;We're hiring!&nbsp;</a> | <a href="/index/faq">&nbsp;How it works&nbsp;</a> | <a href="/index/faq/#showfaqs">&nbsp;FAQs&nbsp;</a> | <a href="/index/privacypolicy">&nbsp;Privacy Policy&nbsp;</a> | <a href="javascript:call_contactbox();">&nbsp;Contact us&nbsp;</a>
		</div>
			<div id="site-generator">
				<?php do_action( 'twentyten_credits' ); ?>
				<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'twentyten' ) ); ?>" title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'twentyten' ); ?>" rel="generator"><?php printf( __( '© 2012 Skillzot. All rights reserved. %s', 'twentyten' ), '' ); ?></a>
			</div><!-- #site-generator -->
			
		</div><!-- #colophon -->
		<div style="border-bottom: 1px dashed #F06142;height:1px;float:left;width:100%;"></div>
	</div><!-- #footer -->
	
</div><!-- #wrapper -->

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
</body>
</html>