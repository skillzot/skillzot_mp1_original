<?php

	/**
	 * @author Rajan Rawal <rajan.rwl@gmail.com>
	 * @version 1.0
	 * @Desc
	 * A custom function to Limit the words in the string so that not allowing word to break abruptly
	 *
	 * @link http://rajan-rawal@blogspot.com
	 * @param str - The string from which we want proper substring
	 * @param limit - The total number of the words from the beginning of the string
	 *
	 * @return boolean - returns string containing the number of words passes as limit from beginning of the string
	 */

class Bankoffercms_Model_Custom_StringLimit{
	
	function text_limit($str,$limit=10){
							
		$str_s = "";
	    if(stripos($str," ")){
	    	
	        $ex_str = explode(" ",$str);
	        if(count($ex_str)>$limit){
	        	
	            for($i=0;$i<$limit;$i++){
	            	
	                $str_s.=$ex_str[$i]." ";
	            }
	            return $str_s;
	        }
	        else{
	        	
	            return $str;
	        }
	    }
	    else{
	    	
	        return $str;
	    }
	}
}
?>