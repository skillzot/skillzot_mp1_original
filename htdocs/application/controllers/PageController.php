<?php

class PageController extends Zend_Controller_Action{

	public function init(){
		$this->_helper->layout()->setLayout("admin");
	}
	
	public function indexAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		
		$authUserNamespace->admin_page_title = "Manage page";
		$pageObj = new Skillzot_Model_DbTable_Pages();
		$this->view->manage_page = $pageObj->fetchAll();
	}
	
	public function addAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		
		$pageObj = new Skillzot_Model_DbTable_Pages();
		
		$pname = $this->_request->getParam("pname");
		
		$this->view->pname = $pname;
		
		if(isset($pname) && $pname!=""){
			$this->view->page = $pageObj->fetchRow("page_name='$pname'");
		}
		
		if(isset($pname) && $pname > 0)$authUserNamespace->admin_page_title = "Edit Page Content";
		else $authUserNamespace->admin_page_title = "Edit Page Content";
		
		if($this->_request->isPost()){
			
			$page_desc = $this->_request->getParam("PageDescription");
		
			/*if($this->_request->isXmlHttpRequest()){
				
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$page_desc = $this->_request->getParam("PageDescription");
				$response = array();
				
				if(strip_tags(trim($page_desc)) == "")$response["data"]["PageDescription"] = "null";
				else $response["data"]["PageDescription"] = "valid";
				
				if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";
				
				echo json_encode($response);
				
			}else{*/
			
				$data = array("page_description"=>$page_desc);
				
				if(isset($pname) && $pname !=""){
				
					$pageObj->update($data,"page_name='$pname'");
					$authUserNamespace->status_message = "Page has been updated successfully";
				}else{
					$merchantObj->insert($data);
					$authUserNamespace->status_message = "Page has been added successfully";
				}
				$this->_redirect("/page");
			//}
		}
	}
	
	public function deleteAction(){
	
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$id = $this->_request->getParam("id");
		
		$pagesObj = new Skillzot_Model_DbTable_Pages();
		$pagesObj->delete("id='$id'");
		$authUserNamespace->status_message = "Page has been deleted successfully";
		$this->_redirect("/page");
	}
}