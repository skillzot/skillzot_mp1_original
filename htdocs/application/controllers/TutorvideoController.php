<?php 
class TutorvideoController extends Zend_Controller_Action
{
	public function init(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if (isset($_COOKIE['username']) && isset($_COOKIE['logintype']) && $_COOKIE['logintype']=='1') {
			//echo "in";
			$authUserNamespace->maintutorid = $_COOKIE['username'];
			$authUserNamespace->logintype = $_COOKIE['logintype'];
		}else if(isset($_COOKIE['username']) && isset($_COOKIE['logintype']) && $_COOKIE['logintype']=='2'){
			//echo "out";
			$authUserNamespace->studentid = $_COOKIE['username'];
			$authUserNamespace->logintype = $_COOKIE['logintype'];
		}
    		
	}

public function addvideosAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
	 	$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
	 	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	 	$tutorAlbumObj = new Skillzot_Model_DbTable_Tutoralbums();
	 	$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
	 	
	 	if (isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid!="")
		{
			$tutor_id = $authUserNamespace->maintutorid;
		}
		else
		{
			$tutor_id = $this->_request->getParam("id");
		}	
	if($this->_request->isPost())
		{
			$videourl = $this->_request->getParam("videourl");
			
			if($this->_request->isXmlHttpRequest())
			{
				
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				if($videourl == "")$response["data"]["videourl"] = "null";
				else $response["data"]["videourl"] = "valid";
				if(!in_array('null',$response['data']))
				{
						$response['returnvalue'] = "success";
				}
				else
				{
					$response['returnvalue'] = "validation";
				}
				echo json_encode($response);			
			}
			else 
			{	
				$lastupdatedate = date("Y-m-d H:i:s");			
				$data = array("tutor_id"=>$tutor_id,"title"=>'',"video_link"=>$videourl,"lastupdatedate"=>$lastupdatedate);
				//print_r($data);exit;
				$tutorVideoObj->insert($data);	
						
				echo "<script>window.parent.location='". BASEPATH ."/editprofile/videos'</script>";
			}
		}	
	}
	
public function editvideonameAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		
		$tutorAlbumObj = new Skillzot_Model_DbTable_Tutoralbums();
		$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
		$video_id = $this->_request->getParam('id'); 
		
		$tutorvideoRow = $tutorVideoObj->fetchRow("id='$video_id'");
		if (isset($tutorvideoRow) && sizeof($tutorvideoRow)>0)
		{
			$videotitle = $tutorvideoRow->title;
			
			$videoid = $tutorvideoRow->id;	
			$this->view->videotitle = $videotitle;										
		}	
		if($this->_request->isPost())
		{
			$videotitle = $this->_request->getParam("videotitle");
			
			if($this->_request->isXmlHttpRequest())
			{
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				if($videotitle == "")$response["data"]["videotitle"] = "null";
				else $response["data"]["albumtitle"] = "valid";
				if(!in_array('null',$response['data']))
				{
						$response['returnvalue'] = "success";
				}
				else
				{
					$response['returnvalue'] = "validation";
				}
				echo json_encode($response);
			}
			else 
			{
				$album_id = $this->_request->getParam('id'); 			
				//$lastupdatedate = date("Y-m-d H:i:s");	
				$data = array("title"=>$videotitle);				
  			    $tutorVideoObj->update($data,"id=$video_id");		
  			    echo "<script>window.parent.location='". BASEPATH ."/editprofile/videos'</script>";						 
			}
		}
	}
public function tutorvideodeleteAction()
	{
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		if(!isset($authUserNamespace->maintutorid) && $authUserNamespace->maintutorid==""){$this->_redirect('/');}
		$tutorphotosObj = new Skillzot_Model_DbTable_Tutorphotos();
		$tutorVideoObj = new Skillzot_Model_DbTable_Tutorvideo();
					
		$videotutorphotos = $this->_request->getParam('id'); 
		
			if(isset($videotutorphotos) && $videotutorphotos!="")
			{								
					$tutorVideoObj->delete("id='$videotutorphotos'");							
					$this->_redirect('/editprofile/videos');
			}
			exit;
	}
	
	
	
}
?>