<?php

class ManagecmsController extends Zend_Controller_Action{
	
	public function aboutusAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$cmsObj = new Skillzot_Model_DbTable_Pages();
		$pageName = $this->_request->getParam("page");
		$cmsData = $cmsObj->fetchRow("page_name='$pageName'");
		if (isset($cmsData) && sizeof($cmsData)>0){
			$description = $cmsData->page_description;
			$this->view->content = $description;
		}
		
	}
	
	public function teamfaqAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->setLayout("searchinnerpage");
		
		
	}
	
	public function howitworksAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$cmsObj = new Skillzot_Model_DbTable_Pages();
		$pageName = $this->_request->getParam("page");
		$cmsData = $cmsObj->fetchRow("page_name='$pageName'");
		if (isset($cmsData) && sizeof($cmsData)>0){
			$description = $cmsData->page_description;
			$this->view->content = $description;
		}
		
	}
	public function faqAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$cmsObj = new Skillzot_Model_DbTable_Pages();
		$pageName = $this->_request->getParam("page");
		$cmsData = $cmsObj->fetchRow("page_name='$pageName'");
		if (isset($cmsData) && sizeof($cmsData)>0){
			$description = $cmsData->page_description;
			$this->view->content = $description;
		}
		
	}
	public function privacypolicyAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$cmsObj = new Skillzot_Model_DbTable_Pages();
		$pageName = $this->_request->getParam("page");
		$cmsData = $cmsObj->fetchRow("page_name='$pageName'");
		if (isset($cmsData) && sizeof($cmsData)>0){
			$description = $cmsData->page_description;
			$this->view->content = $description;
		}
	}
	public function sitemapAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$cmsObj = new Skillzot_Model_DbTable_Pages();
		$pageName = $this->_request->getParam("page");
		$cmsData = $cmsObj->fetchRow("page_name='$pageName'");
		if (isset($cmsData) && sizeof($cmsData)>0){
			$description = $cmsData->page_description;
			$this->view->content = $description;
		}
		
	}
	public function termsconditionAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$cmsObj = new Skillzot_Model_DbTable_Pages();
		$pageName = $this->_request->getParam("page");
		$cmsData = $cmsObj->fetchRow("page_name='$pageName'");
		if (isset($cmsData) && sizeof($cmsData)>0){
			$description = $cmsData->page_description;
			$this->view->content = $description;
		}
	}
	
	public function contactusAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$cmsObj = new Skillzot_Model_DbTable_Pages();
		$pageName = $this->_request->getParam("page");
		$cmsData = $cmsObj->fetchRow("page_name='$pageName'");
		if (isset($cmsData) && sizeof($cmsData)>0){
			$description = $cmsData->page_description;
			$this->view->content = $description;
		}
	}
	public function contactusnewAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$cmsObj = new Skillzot_Model_DbTable_Pages();
		$pageName = $this->_request->getParam("page");
		$cmsData = $cmsObj->fetchRow("page_name='$pageName'");
		if (isset($cmsData) && sizeof($cmsData)>0){
			$description = $cmsData->page_description;
			$this->view->content = $description;
		}
	}
public function wehiringAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		$cmsObj = new Skillzot_Model_DbTable_Pages();
		$pageName = $this->_request->getParam("page");
		$cmsData = $cmsObj->fetchRow("page_name='$pageName'");
		if (isset($cmsData) && sizeof($cmsData)>0){
			$description = $cmsData->page_description;
			$this->view->content = $description;
		}
		
	}
	public function emailusAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		
			$Id = $this->_request->getParam("tutorid");
	 		$this->view->tutorid = $Id;
			if($this->_request->isPost()){
			$UserFName=$this->_request->getParam("UserFName");
			$UserLName=$this->_request->getParam("UserLName");
			$UsercityName = $this->_request->getParam("UsercityName");
			$UserEmail=$this->_request->getParam("UserEmail");
			$UserComments=$this->_request->getParam("UserComments");
			$TutorId = $this->_request->getParam("tutorId");
		
			
			if($this->_request->isXmlHttpRequest()){
				
					$this->_helper->layout()->disableLayout();
					$this->_helper->viewRenderer->setNoRender(true);
					$response=array();
					
					if($UserFName == "")$response["data"]["UserFName"] = "null";
					else $response["data"]["UserFName"] = "valid";
					
					if($UserLName == "")$response["data"]["UserLName"] = "null";
					else $response["data"]["UserLName"] = "valid";
					
					if($UsercityName == "")$response["data"]["UsercityName"] = "null";
					else $response["data"]["UsercityName"] = "valid";
					
					if($UserComments == "")$response["data"]["UserComments"] = "null";
					else $response["data"]["UserComments"] = "valid";
					
					if($UserEmail == "")$response["data"]["UserEmail"] = "null";
					elseif(!preg_match("/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/", $UserEmail)) $response['data']['UserEmail'] = "invalid";
					else $response["data"]["UserEmail"] = "valid";
					
					if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
					else $response['returnvalue'] = "validation";
					echo json_encode($response);
				}
				else {
					$lastupdatedate = date("Y-m-d H:i:s");
					$message1 = "<table width=30% border='1' style='margin:0px;color:#666666;font-size: 12px; '>
										<tr align='center' width=100%>
											<td  width=30%>First name:</td>
											<td  width=30%>".$UserFName."</td>
										</tr>
										<tr align='center'>
											<td>Last name:</td>
											<td width=30%>".$UserLName."</td>
										</tr>
										<tr align='center'>
											<td>Email:</td>
											<td  width=30%>".$UserEmail."</td>
										</tr>
										<tr align='center'>
											<td>City name:</td>
											<td  width=30%>".$UsercityName."</td>
										</tr>
										<tr align='center'>
											<td>Comments:</td>
											<td  width=30%>".$UserComments."</td>
										</tr>
										</table>";
					$UserFullname = $UserFName." ".$UserLName;
					$message = $message1;
					$to = "zot@skillzot.com";
					//$to1 ="sandeep.burman@skillzot.com";
					$to1 ="zot@skillzot.com";
					//$to = "jinu97@gmail.com";
					$from = $UserEmail;
					$mail = new Zend_Mail();
					$mail->setBodyHtml($message);
					$mail->setFrom($from,$UserFullname);
					$mail->addTo($to,'Admin');
					$mail->addTo($to1,'Admin');
					$mail->setSubject('Email your question or problem');//ask
					$mail->send();
					
					$message2 = "<div style='text-align:justify;'>";
					$message2 .= "<div>Hi ".$UserFullname."...<br/></div><br/>";
					$message2 .= "<div>This email is to confirm that we've received your email and will get back to you within 1 working day.<br/></div><br/>";
					$message2 .= "<div>In case you need a faster resolution feel free to call us on +91 9820921404. We are available between 10am to 5:30pm, Mon-Fri.<br/></div><br/>";
					$message2 .= "<div>Here's to learning what you love!<br/></div><br/><br/>";
					$message2 .= "<div>Cheers,<br/></div>";
					$message2 .= "<div>Sandeep<br/></div>";
					$message2 .= "<div>Co-founder @ Skillzot</div>";
					$message2 .= "</div>";
					$message2 .= "<div><a href='http://skillzot.com' style='color:#1AA7EF;'>www.skillzot.com</a></div>";
				
//					print_r($message2);exit;
					$message = $message2;
					$to = $UserEmail;
					$from = "zot@skillzot.com";
					$mail = new Zend_Mail();
					$mail->setBodyHtml($message);
					$mail->setFrom($from,"Sandeep @ Skillzot");
					$mail->addTo($to,$UserFullname);//ask
					$mail->setSubject("We've received your query");
					$mail->send();
					
					$contactusMail = new Skillzot_Model_DbTable_Contactusmail();
					$data = array("c_firstname"=>$UserFName,"c_lastname"=>$UserLName,"c_email"=>$UserEmail,"c_city"=>$UsercityName,"c_comment"=>$UserComments,"c_type"=>"1","lastupdatedate"=>$lastupdatedate);
					$contactusMail->insert($data);
					
					//$this->view->message='Mail has been sent to your EmailId';
					//$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
					//$tutorProfile_row = $tutorProfile->fetchRow($tutorProfile->select()
											// ->where("email='$txtemail'"));
				$this->_redirect('/managecms/finalmessage');
					
				}
		}
		
	}
public function finalmessageAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		//$this->_helper->layout()->setLayout("lightbox");
	}
public function suggetionfinalmessageAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		//$this->_helper->layout()->setLayout("lightbox");
	}
public function sendsuggetionAction(){
	
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->layout()->disableLayout();
			$Id = $this->_request->getParam("tutorid");
			$textdisp = $this->_request->getParam("textdisp","0");
			$coupleDetail = $this->_request->getParam("coupledetail");//from url
			if (isset($coupleDetail) && $coupleDetail!="")
			{
				$this->view->coupledetail = $coupleDetail;
			}
			if($textdisp=="1"){
				$this->view->textdisp = $textdisp;
			}
			if($textdisp=="0"){
				$this->view->textdisp = $textdisp;
			}
	 		$this->view->tutorid = $Id;
			if($this->_request->isPost()){
				$UserFName=$this->_request->getParam("UserFName");
				$UserLName=$this->_request->getParam("UserLName");
				$UsercityName = $this->_request->getParam("UsercityName");
				$UserEmail=$this->_request->getParam("UserEmail");
				$UserComments=$this->_request->getParam("UserComments");
				$TutorId = $this->_request->getParam("tutorId");
				$coupledetailFlag = $this->_request->getParam("coupledetailflag");
				$tellingusFlag = $this->_request->getParam("tellingusflag");
			
				if($this->_request->isXmlHttpRequest()){
					
						$this->_helper->layout()->disableLayout();
						$this->_helper->viewRenderer->setNoRender(true);
						$response=array();
						
						if($UserFName == "")$response["data"]["UserFName"] = "null";
						else $response["data"]["UserFName"] = "valid";
						
						if($UserLName == "")$response["data"]["UserLName"] = "null";
						else $response["data"]["UserLName"] = "valid";
						
						if($UsercityName == "")$response["data"]["UsercityName"] = "null";
						else $response["data"]["UsercityName"] = "valid";
										
						if($UserComments == "")$response["data"]["UserComments"] = "null";
						else $response["data"]["UserComments"] = "valid";
						
						if($UserEmail == "")$response["data"]["UserEmail"] = "null";
						elseif(!preg_match("/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/", $UserEmail)) $response['data']['UserEmail'] = "invalid";
						else $response["data"]["UserEmail"] = "valid";
						
						if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
						else $response['returnvalue'] = "validation";
						echo json_encode($response);
					}else {
							//echo "d".$coupledetailFlag;exit;
						$lastupdatedate = date("Y-m-d H:i:s");
						$message1 = "<table width=30% border='1' style='margin:0px;color:#666666;font-size: 12px; '>
											<tr align='center' width=100%>
												<td  width=30%>First name:</td>
												<td  width=30%>".$UserFName."</td>
											</tr>
											<tr align='center'>
												<td>Last name:</td>
												<td width=30%>".$UserLName."</td>
											</tr>
											<tr align='center'>
												<td>Email:</td>
												<td  width=30%>".$UserEmail."</td>
											</tr>
											<tr align='center'>
												<td>City name:</td>
												<td  width=30%>".$UsercityName."</td>
											</tr>
											<tr align='center'>
												<td>Comments:</td>
												<td  width=30%>".$UserComments."</td>
											</tr>
											</table>";
						if (isset($coupledetailFlag) && $coupledetailFlag=='1')
						{
							$subjectValue = 'Couple of details (Search not found)';
						}else if(isset($tellingusFlag) && $tellingusFlag=='1'){
							$subjectValue = 'Telling us here (< 5 results)';
						}else{
							$subjectValue = 'Send us your suggestion or comment';
						}
						//echo $subjectValue;exit;
						
						$UserFullname = $UserFName." ".$UserLName;
						$message = $message1;
						$to = "zot@skillzot.com";
						//
						//$to1 ="zot@skillzot.com,sandeep.burman@skillzot.com";
						$to1 ="zot@skillzot.com";
						//$to = "jinu97@gmail.com";
						$to2 ="sandeep.burman@skillzot.com";
						$from = $UserEmail;
						$mail = new Zend_Mail();
						$mail->setBodyHtml($message);
						$mail->setFrom($from,$UserFullname); //put user first name,last name
						$mail->addTo($to,'Admin');
						$mail->addTo($to1,'Admin');
						$mail->addTo($to2,'Admin');
						$mail->setSubject($subjectValue); //ask
						$mail->send();
						
						$message2 = "<div style='text-align:justify;'>";
						$message2 .= "<div>Hi ".$UserFullname."...<br/></div><br/>";
						$message2 .= "<div>Thanx once again for taking out time to share your suggestions. We love to hear comments and ideas that can help us help you better.<br/></div><br/>";
						$message2 .= "<div>We have an invisible task force of like a million zots who sift through all the feedback and the ones that keep coming more often get highlighted so that we can prioritize action.<br/></div><br/>";
						$message2 .= "<div>So, we will keep you informed one way or the other, till then, for any specific help, feel free to call us on +91 9820921404. We are available between 10am to 5:30pm, Mon-Fri.<br/></div><br/>";
						$message2 .= "<div>Here's to learning what you love!<br/></div><br/><br/>";
						$message2 .= "<div>Cheers,<br/></div>";
						$message2 .= "<div>Sandeep<br/></div>";
						$message2 .= "<div>Co-founder @ Skillzot</div>";
						$message2 .= "</div>";
						$message2 .= "<div><a href='http://skillzot.com' style='color:#1AA7EF;'>www.skillzot.com</a></div>";
						
						$message = $message2;
						$to = $UserEmail;
						$from = "zot@skillzot.com";
						$mail = new Zend_Mail();
						$mail->setBodyHtml($message);
						$mail->setFrom($from,"Sandeep @ Skillzot");//ask
						$mail->addTo($to,$UserFullname); //ask
						$mail->setSubject("Thank you for your suggestion");//ask
						$mail->send();
						
			
						
						//-------------
						
						$contactusMail = new Skillzot_Model_DbTable_Contactusmail();
						$data = array("c_firstname"=>$UserFName,"c_lastname"=>$UserLName,"c_email"=>$UserEmail,"c_city"=>$UsercityName,"c_comment"=>$UserComments,"c_type"=>"2","lastupdatedate"=>$lastupdatedate);
						$contactusMail->insert($data);
						
						//$this->view->message='Mail has been sent to your EmailId';
						//$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
						//$tutorProfile_row = $tutorProfile->fetchRow($tutorProfile->select()
												// ->where("email='$txtemail'"));
						$this->_redirect('/managecms/suggetionfinalmessage');
						
					}
				}
		}
}
