<?php

	$merchantObject = new Bankoffercms_Model_DbTable_Merchants();
	$merchant_rows = $merchantObject->fetchAll($merchantObject->select()
															  ->from(array("m"=>DATABASE_PREFIX.'merchants'),array('category_id'))
															  ->where("active='Y'"));
	
	if(isset($merchant_rows) && sizeof($merchant_rows)>0){
		
		$category_ids = "";
		foreach($merchant_rows as $merchant){
			
			if($category_ids=="") $category_ids.=$merchant->category_id;
			else $category_ids.=",".$merchant->category_id;
		}
		if($category_ids!=""){
			
			$category_ids=explode(',',$category_ids);
			$category_ids=array_unique($category_ids);
			$category_ids=implode(',',$category_ids);
			
			$categoryObject = new Bankoffercms_Model_DbTable_Categories();
			$category_rows = $categoryObject->fetchAll($categoryObject->select()
																	  ->from(array('c'=>DATABASE_PREFIX.'categories'),array('id','category_name'))
																	  ->where("id in ($category_ids)"));
																	  
			if(isset($category_rows) && sizeof($category_rows)>0){
				$i=1;
				foreach($category_rows as $category){
?>
				<div class="categoriesdiv">
					<div class="categoriesarrow"><img src="<?php echo BASEPATH;?>/images/earnbonuspoints/arrow.gif" alt="arrow" width="6" height="7" border="0" /></div>
					<div class="categoriestxtnames"><a href="<?php echo BASEPATH;?>/index/merchantincategory/id/<?php echo $category->id;?>" class="categoriestxtnames"><?php echo $category->category_name;?></a></div>
				</div>
				<?php
					if($i<sizeof($category_rows)){
				?>
				<div class="categoriesdivider2">&nbsp;</div>
				<?php
					}
				?>
<?php
					$i++;
				}
			}
		}
	}
?>