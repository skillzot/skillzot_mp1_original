var currentpage = 0;
var arrImg = [];
var arrText = [];
var arrLink = [];
var myTimer;
var pauseTime;
var timeDelay = 5000;
var delayPerslide = 700;
var foot;
var head;
var min;
var bodyheight;
jQuery(window).load(function() {
    $('#skillHolder .imgHolder').css('visibility','visible');
    $('#skillHolder .textAndControlHolder').css('visibility','visible');
    pauseTime = setTimeout('next()', timeDelay);
    if(bodyheight > 450)
	{
		$('#skillHolder').css('height',$('.imgHolder').height());
	}
	else
	{
		$('#skillHolder').css('height',bodyheight);
	}
});
jQuery(document).ready(function(){
	foot = parseInt($('#wholefooter').outerHeight());
	head = parseInt($('#fixedheader').outerHeight());
	min = foot+head;
	bodyheight = $(window).height()-min;
	$('#skillHolder .imgHolder').each(function(){
		arrImg.push($(this));
		arrText.push($(this).find('img').attr('title'));
		arrLink.push($(this).find('a').attr('href'));
		var path = $(this).find('img').attr('src');
		//var margintop = $(this).find('img').height()/4;
		//$(this).find('img').css({'margin-top':(-1)*margintop});
		$('#text a').attr('href',arrLink[currentpage]).html(arrText[currentpage]);
		//$(this).css({'background-image':'url('+path+')','height':bodyheight}).html('');
	});
	
	var sh = $('#search').height();
	if($(window).width() > 980)
	{
		var ldis = ($(window).width()-973)/2;
		//var tdis = ((bodyheight-92)-sh)/2;
 		$('#content	').css({'right':ldis+7,'top':'50px','margin-top':'0'});
	}
	else
	{
		//var tdis = ((bodyheight-92)-sh)/2;
 		$('#content	').css({'left':980-329,'top':'50px','margin-top':'0'});
	}
	//$(arrImg[currentpage]).animate({'z-index':'1'});
	$('#nextBtn').click(function(){
		next();
	});
	$('#prevBtn').click(function(){
		prev();
	});
	$('#pauseBtn').click(function(){
		clearInterval(pauseTime);
		displayPlayControl();
	});
	$('#playBtn').click(function(){
		hidePlayControl();
		next();
	});
	$(arrImg[0]).css({'opacity':'1','z-index':'2'});
});
function displayPlayControl()
{
	$('#pauseBtn').css('display','none');
	$('#playBtn').css('display','block');
};
function hidePlayControl()
{
	$('#playBtn').css('display','none');
	$('#pauseBtn').css('display','block');
};
function next()
{
	hidePlayControl();
	clearInterval(pauseTime);
	$('#text a').fadeOut();
	currentpage++;
	if(currentpage < arrImg.length)
	{
		$(arrImg[currentpage]).css({'opacity':'1','z-index':'1'});
		$('#text a').css('display','none').attr('href',arrLink[currentpage]).html(arrText[currentpage]).fadeIn();
		$(arrImg[currentpage-1]).animate({'opacity':'0','z-index':'1'}, delayPerslide);
		$(arrImg[currentpage]).animate({'opacity':'1','z-index':'2'},delayPerslide-100);
		pauseTime = setTimeout('next()', timeDelay);	
	}
	else
	{
		$(arrImg[arrImg.length-1]).animate({'opacity':'0','z-index':'1'},delayPerslide);
		currentpage=1;
		prev();
	}
}
function prev()
{
	hidePlayControl();
	clearInterval(pauseTime);
	$('#text a').fadeOut();
	currentpage--;
	if(currentpage > -1)
	{
		$(arrImg[currentpage]).css({'opacity':'1','z-index':'1'});
		$('#text a').css('display','none').attr('href',arrLink[currentpage]).html(arrText[currentpage]).fadeIn();
		$(arrImg[currentpage+1]).animate({'opacity':'0','z-index':'1'},delayPerslide);
		$(arrImg[currentpage]).animate({'opacity':'1','z-index':'2'},delayPerslide-100);	
		pauseTime = setTimeout('next()', timeDelay);
	}
	else
	{
		$(arrImg[0]).animate({'opacity':'0','z-index':'1'},delayPerslide);
		currentpage=arrImg.length-2;
		next();
	}
}